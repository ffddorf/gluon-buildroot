FROM debian:10

RUN apt-get update
RUN apt-get install build-essential git-core subversion gawk libncurses5-dev lib32z1-dev libssl-dev unzip wget python2 python3 python3-pip time sudo -y
RUN pip3 install gcloud
RUN useradd -d /buildroot -m user
RUN echo "user ALL=(root) NOPASSWD:ALL" > /etc/sudoers.d/user
RUN chmod 0440 /etc/sudoers.d/user

ENTRYPOINT ["su", "-", "user", "-c", "/bin/bash"]

CMD ["su", "-", "user", "-c", "/bin/bash"]
